import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlButton implements ActionListener{
    protected Model model;
    protected Vue vue;

    public ControlButton(Model model, Vue vue) {
        this.model = model;
        this.vue = vue;

        vue.setControlButton(this);
    }

    public void actionPerformed(ActionEvent e){

        model.file="Images/Custom1";

        model.initAttrib();

        for (int i = 0; i < vue.bChoix.length; i++) {
            if (e.getSource() == vue.bChoix[i]){
                if (i+1 > 5){
                    JOptionPane.showMessageDialog(vue, "Il ne peut y avoir qu'entre 1 et 5 joueurs",
                            "avertissement",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                }
                model.setNbJoueur(i+1);
                model.initPioche();
                model.initJoueurs();
                vue.nouvellePartie();
            }
        }
    }
}