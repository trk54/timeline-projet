package brouil;
public class Launcher {
    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Model model = new Model();
                @SuppressWarnings("unused")
				ControlGroup control = new ControlGroup(model);
            }
        });
    }
}